# Poker Admin

A web-app built with Laravel 5.3 created for transactions management; create transactions (multi-transaction), manage related customer/member, related bank accounts, and generate report.

## Requirements

1. All requirements of Laravel 5.3
2. NodeJS (NPM, Gulp)

## Installation

1. Clone this repository
2. Copy the content of '.env.example' to '.env' (create '.env' first)
3. Set database configurations in '.env'
4. Run command 'composer install' (install all PHP dependencies)
6. Run command 'php artisan key:generate' (generate application key)
6. Run command 'php artisan migrate' (create all required tables)
7. Run command 'php artisan db:seed' (insert default data)
8. Run command 'npm install' (install NodeJS dependencies)
9. Run command 'gulp --production' (compile all required assets)
10. Start the app!
