<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MainController@index');
Route::post('/auth/login', 'MainController@doLogin')->name('auth.login');
Route::get('/auth/logout', 'MainController@doLogout')->name('auth.logout');

// Dashboard
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', 'MainController@dashboard')->name('dashboard');

    // Members
    Route::get('members', 'MemberController@index')->name('members.index');
    Route::get('members/add', 'MemberController@create')->name('members.create');
    Route::post('members/add', 'MemberController@store')->name('members.store');
    Route::get('members/isUsernameUnique', 'MemberController@isUsernameUnique')->name('members.isUsernameUnique');
    Route::get('members/{id}', 'MemberController@edit')->name('members.edit');
    Route::patch('members/{id}', 'MemberController@update')->name('members.update');
    Route::delete('members/{id}', 'MemberController@destroy')->name('members.destroy');

    Route::group(['middleware' => 'admin_supervisor'], function () {
        // Users
        Route::get('users', 'UserController@index')->name('users.index');
        Route::get('users/add', 'UserController@create')->name('users.create');
        Route::post('users/add', 'UserController@store')->name('users.store');
        Route::get('users/{id}', 'UserController@edit')->name('users.edit');
        Route::patch('users/{id}', 'UserController@update')->name('users.update');
        Route::delete('users/{id}', ['uses' => 'UserController@destroy', 'middleware' => ['admin', 'supervisor']])->name('users.destroy');

        Route::group(['middleware' => ['admin_supervisor']], function () {
            // Banks
            Route::get('banks', 'BankController@index')->name('banks.index');
            Route::get('banks/add', 'BankController@create')->name('banks.create');
            Route::post('banks', 'BankController@store')->name('banks.store');
            Route::get('banks/{id}', 'BankController@edit')->name('banks.edit');
            Route::patch('banks/{id}', 'BankController@update')->name('banks.update');
            Route::delete('banks/{id}', 'BankController@destroy')->name('banks.destroy');

            // Bank Accounts
            Route::get('bank-accounts', 'BankAccountController@index')->name('bank-accounts.index');
            Route::get('bank-accounts/add', 'BankAccountController@create')->name('bank-accounts.create');
            Route::post('bank-accounts/add', 'BankAccountController@store')->name('bank-accounts.store');
            Route::get('bank-accounts/{id}', 'BankAccountController@edit')->name('bank-accounts.edit');
            Route::patch('bank-accounts/{id}', 'BankAccountController@update')->name('bank-accounts.update');
            Route::delete('bank-accounts/{id}', 'BankAccountController@destroy')->name('bank-accounts.destroy');
        });
    });

    // Deposits
    Route::get('deposit', 'DepositController@index')->name('deposits.index');
    Route::get('deposit/transaction', 'DepositController@transaction')->name('deposits.transaction');
    Route::post('deposit/transaction', 'DepositController@store')->name('deposits.store');
    Route::get('deposit/transaction/{id}', 'DepositController@view')->name('deposits.view');
    Route::get('deposit/transaction/{id}/edit', 'DepositController@edit')->name('deposits.edit');
    Route::patch('deposit/transaction/{id}/edit', 'DepositController@update')->name('deposits.update');
    Route::delete('deposit/transaction/{id}', ['uses' => 'DepositController@destroy', 'middleware' => ['admin', 'supervisor']])->name('deposits.destroy');
    Route::get('deposit/search', 'DepositController@advSearch')->name('deposits.adv_search');
    Route::post('deposit/bulk-action', 'DepositController@bulkAction')->name('deposits.bulk_action');

    // Withdrawal
    Route::get('withdrawal', 'WithdrawalController@index')->name('withdrawals.index');
    Route::get('withdrawal/transaction', 'WithdrawalController@transaction')->name('withdrawals.transaction');
    Route::post('withdrawal/transaction', 'WithdrawalController@store')->name('withdrawals.store');
    Route::get('withdrawal/transaction/{id}', 'WithdrawalController@view')->name('withdrawals.view');
    Route::get('withdrawal/transaction/{id}/edit', 'WithdrawalController@edit')->name('withdrawals.edit');
    Route::patch('withdrawal/transaction/{id}/edit', 'WithdrawalController@update')->name('withdrawals.update');
    Route::delete('withdrawal/transaction/{id}', ['uses' => 'WithdrawalController@destroy', 'middleware' => ['admin', 'supervisor']])->name('withdrawals.destroy');
    Route::get('withdrawal/search', 'WithdrawalController@advSearch')->name('withdrawals.adv_search');
    Route::get('withdrawal/search', 'withdrawalController@advSearch')->name('withdrawals.adv_search');
    Route::post('withdrawal/bulk-action', 'WithdrawalController@bulkAction')->name('withdrawals.bulk_action');

    // Report
    Route::get('report', 'ReportController@report')->name('reports.report');
    Route::get('report/generate', 'ReportController@generate')->name('reports.generate');
    Route::get('report/verification', 'ReportController@verification')->name('reports.verification');
    Route::post('report/verification', 'ReportController@verifyTransactions')->name('reports.verify_transactions');

    //Configuration
    Route::group(['middleware' => 'admin_supervisor'], function () {
        Route::get('configurations', 'ConfigurationController@index')->name('configurations.index');
        Route::get('configurations/add', 'ConfigurationController@create')->name('configurations.create');
        Route::post('configurations/add', 'ConfigurationController@store')->name('configurations.store');
        Route::get('configurations/{id}', 'ConfigurationController@edit')->name('configurations.edit');
        Route::patch('configurations/{id}', 'ConfigurationController@update')->name('configurations.update');
        Route::delete('configurations/{id}', 'ConfigurationController@destroy')->name('configurations.destroy');
    });
});

Route::group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'auth'], function () {
    Route::get('members', 'MemberController@index')->name('api.members.index');
    Route::get('members/{id}', 'MemberController@get')->name('api.members.get');
    Route::get('deposit', 'DepositController@index')->name('api.deposits.get');
    Route::delete('deposit/{id}', 'DepositController@destroy')->name('api.deposits.destroy');
    Route::post('deposit/bulk-action', 'DepositController@bulkAction')->name('api.deposits.bulk_action');
    Route::get('reports/get-by-month', 'ReportController@getByMonth')->name('api.reports.get_by_month');
    Route::get('withdrawal', 'WithdrawalController@index')->name('api.withdrawals.get');
    Route::delete('withdrawal/{id}', 'WithdrawalController@destroy')->name('api.withdrawals.destroy');
    Route::post('withdrawal/bulk-action', 'WithdrawalController@bulkAction')->name('api.withdrawals.bulk_action');
});
