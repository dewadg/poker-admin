<?php

use Illuminate\Database\Seeder;

use App\Configuration;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (is_null(Configuration::where('key', 'max_edit_days_limit')->first())) {
            Configuration::create([
                'key' => 'max_edit_days_limit',
                'value' => 5
            ]);
        }

        if (is_null(Configuration::where('key', 'app_title')->first())) {
            Configuration::create([
                'key' => 'app_title',
                'value' => 'Poker Admin'
            ]);
        }

        if (is_null(Configuration::where('key', 'app_abbr')->first())) {
            Configuration::create([
                'key' => 'app_abbr',
                'value' => 'PA'
            ]);
        }

        if (is_null(Configuration::where('key', 'app_theme')->first())) {
            Configuration::create([
                'key' => 'app_theme',
                'value' => 'skin-green-light'
            ]);
        }
    }
}
