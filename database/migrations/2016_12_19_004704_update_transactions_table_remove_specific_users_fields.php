<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionsTableRemoveSpecificUsersFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function ($table) {
            $table->dropColumn('completed_by');
            $table->dropColumn('set_pending_by');
            $table->dropColumn('canceled_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function ($table) {
            $table->integer('completed_by')->unsigned()->nullable();
            $table->integer('set_pending_by')->unsigned()->nullable();
            $table->integer('canceled_by')->unsigned()->nullable();
        });
    }
}
