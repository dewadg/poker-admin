<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function ($table) {
            $table->dropColumn('member_full_name');
            $table->dropColumn('member_phone');
            $table->dropColumn('member_email');
            $table->dropColumn('member_bank');
            $table->dropColumn('member_bank_account_number');
            $table->dropColumn('member_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function ($table) {
            $table->string('member_full_name');
            $table->string('member_phone');
            $table->string('member_email');
            $table->string('member_bank');
            $table->string('member_bank_account_number');
            $table->string('member_username');
        });
    }
}
