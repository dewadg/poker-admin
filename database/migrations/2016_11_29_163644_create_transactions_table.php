<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', [1, 2]);
            $table->string('member_user_id');
            $table->string('member_full_name');
            $table->string('member_phone');
            $table->string('member_email');
            $table->string('member_bank');
            $table->string('member_bank_account_number');
            $table->string('bank');
            $table->string('bank_account_number');
            $table->text('note');
            $table->decimal('amount', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
