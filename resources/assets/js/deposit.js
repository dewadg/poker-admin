import angular from 'angular'
import ui_select from 'ui-select'
import ngSanitize from 'angular-sanitize'

angular.module('deposit', ['ui.select', 'ngSanitize'])

.controller('DepositController', ['$scope', '$http', ($scope, $http) => {
    $scope.global = {}
    $scope.global.numOfTrans = 1
    $scope.transactions = [
        {
            index: 1,
            member: null,
            member_id: null,
            member_username: null,
            member_full_name: null,
            member_bank: null,
            member_bank_account_number: null,
            member_email: null,
            member_phone: null,
            bank_account_id: null,
            status: "1",
            note: '',
            amount: null
        }
    ]

    $http({
        method: 'GET',
        url: '/api/members?fields=id,username'
    }).then(response => {
        $scope.members = response.data
    }, error => {
        swal("Error", "Error while loading member\'s data", "error")
    })

    $scope.addNewTrans = () => {
        $scope.global.numOfTrans++;
        $scope.transactions.push({
            index: $scope.global.numOfTrans,
            member_id: null,
            member_username: null,
            member_full_name: null,
            member_bank: null,
            member_bank_account_number: null,
            member_email: null,
            member_phone: null,
            bank_account_id: null,
            status: "1",
            note: '',
            amount: null
        })
    }

    $scope.commit = () => {
        $http({
            method: 'POST',
            url: '/dashboard/deposit/transaction',
            data: {
                transactions: $scope.transactions
            }
        }).then(response => {
            if (response.data.status == 200) {
                swal({
                    type: 'success',
                    title: 'Succeed',
                    text: 'Add deposit transaction(s) successfully committed',
                    closeOnConfirm: false
                }, () => {
                    window.location = '/dashboard/deposit'
                });
            } else if (response.data.status == 500) {
                swal("Error", "Fill all the required fields before committing", "error")
            }
        }, error => {
            swal("Error", "Transaction failed to send", "error")
        })
    }
}])

.controller('TransactionController', ['$scope', '$http', function ($scope, $http) {
    $scope.removeTransaction = () => {
        $scope.global.numOfTrans--
        $scope.transaction.index = -1;
    }

    $scope.$watch('transaction.member', (newVal, oldVal) => {
        if (newVal !== oldVal) {
            $http({
                method: 'GET',
                url: '/api/members/' + newVal.id
            }).then(response => {
                let member = response.data

                $scope.transaction.member_id = member.id
                $scope.transaction.member_full_name = member.full_name
                $scope.transaction.member_username = member.username
                $scope.transaction.member_bank = member.bank.name
                $scope.transaction.member_bank_account_number = member.bank_account_number
                $scope.transaction.member_email = member.email
                $scope.transaction.member_phone = member.phone
            }, error => {
                swal("Error", "Error while loading member\'s data", "error")
            })
        }
    })
}])
