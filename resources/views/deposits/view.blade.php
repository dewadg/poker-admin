@extends('templates.dashboard')

@section('page_title', 'View Deposit Transaction')
@section('content')
<section class="content-header">
    <h1>Transaction Details</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('deposits.index') }}"><i class="fa fa-plus-circle"></i> Deposit History</a></li>
        <li><a href="{{ route('deposits.view', $deposit->id) }}">Details</a></li>
    </ol>
</section>
<section class="invoice">
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header">
                Transaction Details
                <small class="pull-right">{{ date_format(date_create($deposit->created_at), 'j F Y @ H:i:s') }}</small>
            </h2>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Member,
            <address>
                <strong>{{ $deposit->member->username }}</strong><br>
                {{ $deposit->member->full_name }}<br>
                {{ $deposit->member->email }}<br>
                {{ $deposit->member->phone }}<br>
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>Processed at</td>
                        <td>{{ $deposit->created_at }}</td>
                    </tr>
                    @if(! empty($deposit->completed_at))
                        <tr>
                            <td>Completed at</td>
                            <td>{{ $deposit->completed_at }}</td>
                        </tr>
                    @elseif(! empty($deposit->pending_at))
                        <tr>
                            <td>Pending at</td>
                            <td>{{ $deposit->pending_at }}</td>
                        </tr>
                    @elseif(! empty($deposit->canceled_at))
                        <tr>
                            <td>Canceled at</td>
                            <td>{{ $deposit->canceled_at }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="col-sm-4 invoice-col">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>Processed by</td>
                        <td>{{ $deposit->user->name }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Transferred From</th>
                        <th>Transferred To</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $deposit->member_bank }} | {{ $deposit->member_bank_account_number }}</td>
                        <td>{{ $deposit->bank }} | {{ $deposit->bank_account_number }}</td>
                        <td>Rp {{ number_format($deposit->amount, 0, ',', '.') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
