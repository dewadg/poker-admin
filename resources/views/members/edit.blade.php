@extends('templates.dashboard')

@push('custom_scripts')
<script>
    function isUsernameUnique(username) {
        $.get('{{ route('members.isUsernameUnique') }}', {username: username}, function (response) {
            if (! response) {
                alert('Username already taken');
            }
        });
    }
</script>
@endpush

@section('page_title', 'Edit Member: ' . $member->username)
@section('content')
<section class="content-header">
    <h1>Edit Member: {{ $member->username }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('members.index') }}"><i class="fa fa-users"></i> Members</a></li>
        <li><a href="{{ route('members.create') }}"><i class="fa fa-users"></i> Edit Member: {{ $member->username }}</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Fill the form below</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <form action="{{ route('members.update', $member->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" value="{{ $member->username }}" readonly>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-6">
                        <input type="text" name="email" class="form-control" value="{{ $member->email }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="full_name" class="form-control" value="{{ $member->full_name }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-6">
                        <input type="text" name="phone" class="form-control" maxlength="12" value="{{ $member->phone }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="bank_id" class="col-sm-2 control-label">Bank</label>
                    <div class="col-sm-6">
                        <select name="bank_id" class="form-control">
                            @foreach($banks as $bank)
                                <option value="{{ $bank->id }}" {{ $member->bank->id == $bank->id ? 'selected' : null }}>{{ $bank->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="bank_account_number" class="col-sm-2 control-label">Account Number</label>
                    <div class="col-sm-6">
                        <input type="text" name="bank_account_number" class="form-control" value="{{ $member->bank_account_number }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
