@extends('templates.dashboard')

@push('custom_style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush

@push('custom_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#members').DataTable();
    });

    $('.deleteBtn').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        swal({
            title: "Warning",
            text: "Are you really sure want to delete this?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            $('#deleteForm-' + id).submit();
        });
    });
</script>
@endpush

@section('page_title', 'Members')
@section('content')
<section class="content-header">
    <h1>Members List</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('members.index') }}"><i class="fa fa-users"></i> Members</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <a href="{{ route('members.create') }}" class="btn btn-primary" style="margin: 0 0 25px 0">Add New Member</a>
            <table id="members" class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <td>Full Name</td>
                        <td>Username</td>
                        <td>Email</td>
                        <td>Bank</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($members as $member)
                        <tr>
                            <td>{{ $member->full_name }}</td>
                            <td>{{ $member->username }}</td>
                            <td>{{ $member->email }}</td>
                            <td>{{ $member->bank->name }} - {{ $member->bank_account_number }}</td>
                            <td>
                                @if($member->status == 1)
                                    <span class="label label-success">Active</span>
                                @elseif($member->status == 2)
                                    <span class="label label-default">Inactive</span>
                                @elseif($member->status == 3)
                                    <span class="label label-warning">Email Activation Pending</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('members.edit', $member->id) }}" class="btn btn-xs btn-primary">Edit</a>
                                <form id="deleteForm-{{ $member->id }}" action="{{ route('members.destroy', $member->id) }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="deleteBtn btn btn-xs btn-danger" data-id="{{ $member->id }}">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
