@extends('templates.dashboard')

@push('custom_style')
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
@endpush

@push('custom_scripts')
<!-- bootstrap color picker -->
<script src="{{ asset('bower_components/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script>
$(function () {
    $('#color').colorpicker();
});
</script>
@endpush

@section('page_title', 'Edit Bank: ' . $bank->name)
@section('content')
<section class="content-header">
    <h1>Edit Bank: {{ $bank->name }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('banks.index') }}"><i class="fa fa-bank"></i> Bank Accounts</a></li>
        <li><a href="{{ route('banks.edit', $bank->id) }}">Edit Bank: {{ $bank->name }}</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Fill the form below</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <form action="{{ route('banks.update', $bank->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Bank Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" class="form-control" value="{{ $bank->name }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="color" class="col-sm-2 control-label">Color (required in report)</label>
                    <div class="col-sm-6">
                        <input type="text" name="color" id="color" class="form-control" value="{{ $bank->color }}" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
