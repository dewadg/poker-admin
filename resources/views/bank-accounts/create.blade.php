@extends('templates.dashboard')

@section('page_title', 'Add New Bank Account')
@section('content')
<section class="content-header">
    <h1>Add New Bank Account</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('bank-accounts.index') }}"><i class="fa fa-bank"></i> Bank Accounts</a></li>
        <li><a href="{{ route('bank-accounts.create') }}">Add New Bank Account</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Fill the form below</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <form action="{{ route('bank-accounts.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="bank_id" class="col-sm-2 control-label">Bank</label>
                    <div class="col-sm-6">
                        <select name="bank_id" class="form-control">
                            @foreach($banks as $bank)
                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="bank_account_number" class="col-sm-2 control-label">Account Number</label>
                    <div class="col-sm-6">
                        <input type="text" name="bank_account_number" class="form-control" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="bank_account_holder" class="col-sm-2 control-label">Account Holder</label>
                    <div class="col-sm-6">
                        <input type="text" name="bank_account_holder" class="form-control" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
