@extends('templates.dashboard')

@push('custom_style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush

@push('custom_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#members').DataTable();
    });

    $('.deleteBtn').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        swal({
            title: "Warning",
            text: "Are you really sure want to delete this?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            $('#deleteForm-' + id).submit();
        });
    });
</script>
@endpush

@section('page_title', 'Bank Accounts')
@section('content')
<section class="content-header">
    <h1>Bank Accounts List</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('bank-accounts.index') }}"><i class="fa fa-bank"></i> Bank Accounts</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <a href="{{ route('bank-accounts.create') }}" class="btn btn-primary" style="margin: 0 0 25px 0">Add New Bank Account</a>
            <table id="members" class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <td>Bank</td>
                        <td>Account Number</td>
                        <td>Holder</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bank_accounts as $account)
                        <tr>
                            <td>{{ $account->bank->name }}</td>
                            <td>{{ $account->bank_account_number }}</td>
                            <td>{{ $account->bank_account_holder }}</td>
                            <td>
                                <a href="{{ route('bank-accounts.edit', $account->id) }}" class="btn btn-xs btn-primary">Edit</a>
                                <form id="deleteForm-{{ $account->id }}" action="{{ route('bank-accounts.destroy', $account->id) }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="deleteBtn btn btn-xs btn-danger" data-id="{{ $account->id }}">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
