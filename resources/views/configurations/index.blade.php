@extends('templates.dashboard')

@push('custom_style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush

@push('custom_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#banks').DataTable();
    });

    $('.deleteBtn').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        swal({
            title: "Warning",
            text: "Are you really sure want to delete this?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            $('#deleteForm-' + id).submit();
        });
    });
</script>
@endpush

@section('page_title', 'Configurations')
@section('content')
<section class="content-header">
    <h1>Configurations List</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('configurations.index') }}"><i class="fa fa-bank"></i> Configurations</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <a href="{{ route('configurations.create') }}" class="btn btn-primary" style="margin: 0 0 25px 0">Add New Configuration</a>
            <table id="banks" class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <td>Key</td>
                        <td>Value</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($configurations as $config)
                        <tr>
                            <td>{{ $config->key }}</td>
                            <td>{{ $config->value }}</td>
                            <td>
                                <a href="{{ route('configurations.edit', $config->id) }}" class="btn btn-xs btn-primary">Edit</a>
                                <form id="deleteForm-{{ $config->id }}" action="{{ route('configurations.destroy', $config->id) }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="deleteBtn btn btn-xs btn-danger" data-id="{{ $config->id }}">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
