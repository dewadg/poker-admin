@extends('templates.dashboard')

@push('custom_style')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/select2/select2.min.css') }}">
@endpush

@push('custom_scripts')
<!-- Select2 -->
<script src="{{ asset('bower_components/AdminLTE/plugins/select2/select2.full.min.js') }}"></script>
<!-- Custom Scripts -->
<script>
    $(document).on('change', '.member-id', function () {
        $.get('/api/members/' + $(this).val(), {}, function (response) {
            $('.member-full-name').val(response.full_name);
            $('.member-bank').val(response.bank.name);
            $('.member-bank-account-number').val(response.bank_account_number);
            $('.member-email').val(response.email);
            $('.member-phone').val(response.phone);
        });
    });

    $(function () {
        $('.member-id').select2();
    });
</script>
@endpush

@section('page_title', 'Edit Transaction')
@section('content')
<section class="content-header">
    <h1>Deposit</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('withdrawals.index') }}"><i class="fa fa-plus-circle"></i> Withdrawals</a></li>
        <li><a href="{{ route('withdrawals.edit', $withdrawal->id) }}">Edit Transaction</a></li>
    </ol>
</section>
<section class="content">
    <form action="{{ route('withdrawals.update', $withdrawal->id) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PATCH">
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-warning">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Member</label>
                            <select name="member_id" class="form-control member-id" style="width: 100%">
                                @foreach($members as $member)
                                    <option value="{{ $member->id }}" {{ $member->id == $withdrawal->member_id ? 'selected' : null }}>{{ $member->username }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label >Full Name</label>
                                <input type="text" class="form-control member-full-name" value="{{ $withdrawal->member->full_name }}" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>Bank</label>
                                    <input type="text" class="form-control member-bank" value="{{ $withdrawal->member->bank->name }}" readonly>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" class="form-control member-bank-account-number" value="{{ $withdrawal->member->bank_account_number }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control member-email" value="{{ $withdrawal->member->email }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control member-phone" value="{{ $withdrawal->member->phone }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bank_account_id">Bank</label>
                                    <select name="bank_account_id" class="form-control">
                                        @foreach($bank_accounts as $account)
                                            <option value="{{ $account->id }}" {{ $account->id == $withdrawal->bank_account_id ? 'selected' : null }}>
                                                {{ $account->bank->name }} - {{ $account->bank_account_number }} - {{ $account->bank_account_holder }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" class="form-control">
                                        <option value="1" {{ $withdrawal->status == '1' ? 'selected' : null }}>Complete</option>
                                        <option value="2"{{ $withdrawal->status == '2' ? 'selected' : null }}>Pending</option>
                                        <option value="3"{{ $withdrawal->status == '3' ? 'selected' : null }}>Canceled</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" class="form-control">{{ $withdrawal->note }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input name="amount" type="text" class="form-control input-lg pull-right amount" value="{{ number_format($withdrawal->amount, 0, '', '') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success pull-right">Save Changes</button>
    </form>
</section>
@endsection
