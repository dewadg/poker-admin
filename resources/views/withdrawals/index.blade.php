@extends('templates.dashboard')

@push('custom_scripts')
<script>
    var custom_left_toolbars = '<select id="dt-action" class="input-sm form-control">' +
    '<option value="1">Set Complete</option>';

    @if(Auth::user()->type == 'Administrator')
        custom_left_toolbars += '<option value="2">Delete</option>';
    @endif

    custom_left_toolbars += '</select><button type="button" id="dt-run-action" class="btn btn-primary btn-sm">Run</button>';

    var custom_right_toolbars = '<select id="dt-status" class="input-sm form-control">' +
    '<option value="*">All</option>' +
    '<option value="1">Completed</option>' +
    '<option value="2">Pending</option>' +
    '<option value="3">Canceled</option>' +
    '</select>' +
    '<input type="text" id="dt-daterange" class="form-control input-sm"><button type="button" id="dt-getbydaterange" class="btn btn-primary btn-sm">Clear</button>';

    $(document).ready(function () {
        var table = $('#withdrawals').DataTable({
            dom: '<"fleft"l> <"fleft left-toolbar"> <"fright"f> <"fright right-toolbar"> <"clearfix"> t <"fleft"i> <"fright"p>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '/api/withdrawal?datatables=1',
                data: function (data) {
                    data.status = $('#dt-status').val();
                    data.daterange = $('#dt-daterange').val();
                }
            },
            ordering: false,
            columns: [
                {
                    data: 'transaction_id',
                    searchable: false,
                    render: function (data, type, full, meta) {
                        return '<input type="checkbox" name="transaction_id[]" class="transaction-id" value="' + data + '">'
                    }
                },
                { data: 'member_username' },
                { data: 'member_full_name' },
                { data: 'member_bank_name' },
                { data: 'user_name'},
                {
                    data: 'transaction_amount',
                    render: function (data, type, full, meta) {
                        return 'Rp ' + numeral(data).format('0,0');
                    }
                },
                {
                    data: 'transaction_note',
                    render: function (data, type, full, meta) {
                        if (data === '') {
                            return '-';
                        }

                        return data;
                    }
                },
                { data: 'transaction_created_at' },
                {
                    data: 'transaction_status',
                    render: function (data, type, full, meta) {
                        if (data === '1') {
                            return '<span class="label label-success">Completed</span>';
                        } else if (data === '2') {
                            return '<span class="label label-warning">Pending</span>';
                        } else if (data === '3') {
                            return '<span class="label label-danger">Canceled</span>';
                        }
                    }
                },
                {
                    data: 'transaction_id',
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var render = '<a href="/dashboard/withdrawal/transaction/' + data + '" class="btn btn-xs btn-primary">View Details</a>';
                        render += '&nbsp;<a href="/dashboard/withdrawal/transaction/' + data + '/edit" class="btn btn-xs btn-primary">Edit</a>';

                        @if(Auth::user()->type != 'Operator')
                            render += '&nbsp;<button type="button" id="destroy-transaction-' + data + '" class="btn btn-xs btn-danger destroy-transaction" data-id="' + data + '">Delete</a>';
                        @endif

                        return render;
                    }
                }
            ]
        });

        $('div.left-toolbar').html(custom_left_toolbars);
        $('div.right-toolbar').html(custom_right_toolbars);

        $('#dt-daterange').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#dt-daterange').val(null);

        $(document).on('click', '.destroy-transaction', function () {
            var id = $(this).attr('data-id');

            swal({
                title: "Warning",
                text: "Are you really sure want to delete this?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function () {
                $.post('/api/withdrawal/' + id, {_method: 'DELETE'}, function (response) {
                    console.log(response);
                    if (response.status == 200)  {
                        table.ajax.reload();

                        swal('Operation Succeed', 'Transaction successfully deleted', 'success');
                    } else if (response.status == 500) {
                        swal('Failed', response.data[0], 'error');
                    }
                });
            });
        })

        $(document).on('change', '#dt-status', function () {
            table.ajax.reload();
        });

        $(document).on('click', '#dt-run-action', function () {
            if ($('.transaction-id:checked').length == 0) {
                swal('No transactions were selected', 'Check desired transactions and try again', 'error');
            } else {
                var action = $('#dt-action').val();
                var transactions = $('.transaction-id:checked').map(function () {
                    return $(this).val();
                });
                var transaction_id = $.map(transactions, function (value, index) {
                    return [value];
                });

                swal({
                    title: 'Bulk Action',
                    text: 'Are you sure to run this bulk action?',
                    type: 'info',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                }, function () {
                    $.post('/api/withdrawal/bulk-action', {action: action, transaction_id: transaction_id}, function (response) {
                        if (response.status == 500) {
                            swal('Bulk Action Failed', 'Error: ' + response.data[0] , 'error');
                        } else if (response.status == 200) {
                            table.ajax.reload();

                            swal('Bulk Action Succeed', 'All transactions successfully processed', 'success');
                        }
                    });
                });
            }
        });

        $('#dt-daterange').on('apply.daterangepicker', function (e, datepicker) {
            table.ajax.reload();
        });

        $('#dt-daterange').on('cancel.daterangepicker', function (e, datepicker) {
            $('#dt-daterange').val(null);
        });

        $(document).on('click', '#dt-getbydaterange', function () {
            $('#dt-daterange').val(null);
            table.ajax.reload();
        });
    });
</script>
@endpush

@section('page_title', 'Withdrawal History' . (isset($sub_title) ? ': ' . $sub_title : null))
@section('content')
<section class="content-header">
    <h1>Withdrawal History <small>{{ isset($sub_title) ? $sub_title : null }}</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('withdrawals.index') }}"><i class="fa fa-plus-circle"></i> Withdrawal History</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <table id="withdrawals" class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <td></td>
                        <td>Member</td>
                        <td>Full Name</td>
                        <td>Bank</td>
                        <td>Processed by</td>
                        <td>Amount</td>
                        <td width="150">Note</td>
                        <td>Date Time</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
@endsection
