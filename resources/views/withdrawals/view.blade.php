@extends('templates.dashboard')

@section('page_title', 'View Withdrawal Transaction')
@section('content')
<section class="content-header">
    <h1>Transaction Details</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('withdrawals.index') }}"><i class="fa fa-fa-minus-circle"></i> Withdrawal History</a></li>
        <li><a href="{{ route('withdrawals.view', $withdrawal->id) }}">Details</a></li>
    </ol>
</section>
<section class="invoice">
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header">
                Transaction Details
                <small class="pull-right">{{ date_format(date_create($withdrawal->created_at), 'j F Y @ H:i:s') }}</small>
            </h2>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Member,
            <address>
                <strong>{{ $withdrawal->member->username }}</strong><br>
                {{ $withdrawal->member->full_name }}<br>
                {{ $withdrawal->member->email }}<br>
                {{ $withdrawal->member->phone }}<br>
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>Processed at</td>
                        <td>{{ $withdrawal->created_at }}</td>
                    </tr>
                    @if(! empty($withdrawal->completed_at))
                        <tr>
                            <td>Completed at</td>
                            <td>{{ $withdrawal->completed_at }}</td>
                        </tr>
                    @elseif(! empty($withdrawal->pending_at))
                        <tr>
                            <td>Pending at</td>
                            <td>{{ $withdrawal->pending_at }}</td>
                        </tr>
                    @elseif(! empty($withdrawal->canceled_at))
                        <tr>
                            <td>Canceled at</td>
                            <td>{{ $withdrawal->canceled_at }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="col-sm-4 invoice-col">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>Processed by</td>
                        <td>{{ $withdrawal->user->name }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Transferred From</th>
                        <th>Transferred To</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $withdrawal->member_bank }} | {{ $withdrawal->member_bank_account_number }}</td>
                        <td>{{ $withdrawal->bank }} | {{ $withdrawal->bank_account_number }}</td>
                        <td>Rp {{ number_format($withdrawal->amount, 0, ',', '.') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
