@extends('templates.dashboard')

@push('custom_style')
<!-- Deposit Angular Module -->
<script src="{{ asset('js/withdrawal.js') }}"></script>
@endpush

@section('page_title', 'Withdrawal')
@section('content')
<section class="content-header">
    <h1>Withdrawal</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('withdrawals.index') }}"><i class="fa fa-minus-circle"></i> Withdrawal</a></li>
        <li><a href="{{ route('withdrawals.transaction') }}">Withdraw</a></li>
    </ol>
</section>
<section ng-app="withdrawal" ng-controller="WithdrawalController" class="content">
    <button ng-click="addNewTrans()" type="button" class="btn btn-primary pull-left" style="margin: 0 0 25px 0">Multi Transaction</button>
    <button ng-click="commit()" type="button" class="btn btn-success pull-right">Save Changes</button>
    <div class="clearfix"></div>

    <div ng-repeat="transaction in transactions" ng-controller="TransactionController" class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Withdraw</h3>
            <div class="box-tools pull-right">
                <button ng-click="removeTransaction()" type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Member</label>
                        <ui-select ng-model="transaction.member">
                            <ui-select-match placeholder="Select desired member">
                                <span ng-bind="$select.selected.username"></select>
                            </ui-select-match>
                            <ui-select-choices repeat="member in members | filter: $select.search track by member.id">
                                <span ng-bind="member.username"></span>
                            </ui-select-choices>
                        </ui-select>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="member_full_name">Full Name</label>
                            <input ng-model="transaction.member_full_name" type="text" class="form-control member-full-name" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="member_bank">Bank</label>
                                <input ng-model="transaction.member_bank" type="text" class="form-control member-bank" readonly>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="member_bank_account_number">Account Number</label>
                                <input ng-model="transaction.member_bank_account_number" type="text" class="form-control member-bank-account-number" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="member_email">Email</label>
                                    <input ng-model="transaction.member_email" type="text" class="form-control member-email" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="member_phone">Phone</label>
                                    <input ng-model="transaction.member_phone" type="text" class="form-control member-phone" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bank_account_id">Bank</label>
                                <select ng-model="transaction.bank_account_id" class="form-control">
                                    <option value="" disabled selected>Choose Bank Account</option>
                                    @foreach($bank_accounts as $account)
                                        <option value="{{ $account->id }}">
                                            {{ $account->bank->name }} - {{ $account->bank_account_number }} - {{ $account->bank_account_holder }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select ng-model="transaction.status" class="form-control">
                                    <option value="1">Complete</option>
                                    <option value="2">Pending</option>
                                    <option value="3">Canceled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea ng-model="transaction.note" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <div class="input-group">
                            <div class="input-group-addon">Rp</div>
                            <input ng-model="transaction.amount" type="text" class="form-control input-lg pull-right amount">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
