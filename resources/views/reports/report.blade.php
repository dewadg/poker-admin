@extends('templates.dashboard')

@push('custom_scripts')
<script>
    $(function () {
        $('#daterange').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        $('#daterange').val(null);
    });
</script>
@endpush

@section('page_title', 'Generate Report')
@section('content')
<section class="content-header">
    <h1>Report</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('reports.report') }}"><i class="fa fa-area-chart"></i> Report</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-body">
                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-warning">
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <form action="{{ route('reports.generate') }}" method="GET">
                        <div class="form-group">
                            <label for="daterange">Start Date</label>
                            <input id="daterange" type="text" name="daterange" class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-primary">Generate</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
