@extends('templates.dashboard')

@push('custom_scripts')
<script>
    $(document).on('change', '#month', function () {
        $.get('{{ route('api.reports.get_by_month') }}', {month: $(this).val()}, function (response) {
            if (response.status == 200) {
                var isGreen = null;
                var isRed = null;

                $('#transactions-list').empty();

                if (response.data.length > 0) {
                    $.each(response.data, function (index, value) {
                        isGreen = value.status == 'green' ? 'checked' : '';
                        isRed = value.status == 'red' ? 'checked' : '';

                        $('#transactions-list').append('<tr><td>' + value.date + '</td><td><input type="hidden" name="transactions[' + index + '][id]" value="' + value.id + '"><label><input type="radio" name="transactions[' + index + '][status]" value="green" ' + isGreen + '> On</label><label><input type="radio" name="transactions[' + index + '][status]" value="red" ' + isRed + '> Off</label></td></tr>');
                    });
                } else {
                    $('#transactions-list').append('<tr><td colspan="2"><center>No data for this month</center></td></tr>');
                }
            }
        });
    });
</script>
@endpush

@section('page_title', 'Validate Transactions')
@section('content')
<section class="content-header">
    <h1>Report</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('reports.report') }}"><i class="fa fa-area-chart"></i> Report</a></li>
        <li><a href="{{ route('reports.verification') }}"><i class="fa fa-area-chart"></i> Validation</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-warning">
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="form-group">
                        <select id="month" class="form-control">
                            <option value="" disabled selected>Select Month</option>
                            @foreach($months as $month)
                                <option value="{{ $month[0] }}">{{ $month[1] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary pull-right" onclick="document.getElementById('transactions-verification').submit()">Run Action</button>
                    </div>
                    <form id="transactions-verification" action="{{ route('reports.verify_transactions') }}" method="POST">
                        {{ csrf_field() }}
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="50%">Date</th>
                                    <th width="50%">Status</th>
                                </tr>
                            </thead>
                            <tbody id="transactions-list">
                                <tr>
                                    <td colspan="2"><center>Select desired month first</center></td>
                                </tr>
                                <!-- <tr>
                                    <td>2 December 2016</td>
                                    <td>
                                        <input type="hidden" name="transactions[1][id]" value="1">
                                        <label>
                                            <input type="radio" name="transactions[1][status]" value="green"> On
                                        </label>
                                        <label>
                                            <input type="radio" name="transactions[1][status]" value="red"> Off
                                        </label>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
