@extends('templates.dashboard')
@section('page_title', 'Forbidden')
@section('content')
<section class="content-header">
</section>
<section class="content">
    <div class="error-page">
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i>&nbsp;&nbsp;Internal Server Error</h3>
            <p>An unexpected error has occured. Please try your request again and if problem persists then contact system administrator</p>
        </div>
      </div>
    </section>
@endsection
