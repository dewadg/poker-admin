@extends('templates.dashboard')
@section('page_title', 'Forbidden')
@section('content')
<section class="content-header">
</section>
<section class="content">
    <div class="error-page">
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i>&nbsp;&nbsp;Access Forbidden</h3>
            <p>You are not allowed to access this page</p>
        </div>
      </div>
    </section>
@endsection
