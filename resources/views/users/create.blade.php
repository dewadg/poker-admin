@extends('templates.dashboard')

@section('page_title', 'Add New User')
@section('content')
<section class="content-header">
    <h1>Add New User</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> Users</a></li>
        <li><a href="{{ route('users.create') }}"><i class="fa fa-users"></i> Add New User</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Fill the form below</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <form action="{{ route('users.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-6">
                        <select name="type" class="form-control">
                            @if(\Auth::user()->type == 'Administrator')
                                <option value="Administrator">Administrator</option>
                                <option value="Supervisor">Supervisor</option>
                            @endif
                            <option value="Operator">Operator</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password" class="form-control" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="password_conf" class="col-sm-2 control-label">Confirm Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password_conf" class="form-control" required>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
