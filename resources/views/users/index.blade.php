@extends('templates.dashboard')

@push('custom_style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush

@push('custom_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#users').DataTable();
    });

    $('.deleteBtn').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        swal({
            title: "Warning",
            text: "Are you really sure want to delete this?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            $('#deleteForm-' + id).submit();
        });
    });
</script>
@endpush

@section('page_title', 'Users')
@section('content')
<section class="content-header">
    <h1>Users List</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> Users</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <a href="{{ route('users.create') }}" class="btn btn-primary" style="margin: 0 0 25px 0">Add New Member</a>
            <table id="users" class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <td>Username</td>
                        <td>Type</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->type }}</td>
                            <td>
                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-primary">Edit</a>
                                <form id="deleteForm-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="deleteBtn btn btn-xs btn-danger" data-id="{{ $user->id }}">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
