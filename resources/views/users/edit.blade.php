@extends('templates.dashboard')

@section('page_title', 'Edit User: ' . $user->name)
@section('content')
<section class="content-header">
    <h1>Edit User: {{ $user->name }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> Users</a></li>
        <li><a href="{{ route('users.create') }}"><i class="fa fa-users"></i> Edit User: {{ $user->name }}</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Fill the form below</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <form action="{{ route('users.update', $user->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" class="form-control" value="{{ $user->name }}" readonly>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-6">
                        <select name="type" class="form-control">
                            @if(\Auth::user()->type == 'Administrator')
                                <option value="Administrator" {{ $user->type == 'Administrator' ? 'selected' : null }}>Administrator</option>
                                <option value="Supervisor" {{ $user->type == 'Supervisor' ? 'selected' : null }}>Supervisor</option>
                            @endif
                            <option value="Operator" {{ $user->type == 'Operator' ? 'selected' : null }}>Operator</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="password_conf" class="col-sm-2 control-label">Confirm Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password_conf" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
