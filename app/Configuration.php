<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'key',
        'value'
    ];

    public $timestamps = false;

    public static function get($key)
    {
        $config = new Configuration;

        return $config->where('key', $key)->first()->value;
    }
}
