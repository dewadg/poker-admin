<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
        'bank_id',
        'bank_account_number',
        'bank_account_holder'
    ];

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
}
