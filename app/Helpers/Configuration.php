<?php

namespace App\Helpers;

use App\Configuration as Config;

class Configuration
{
    public function __construct()
    {
        $this->configurations = Config::all();
    }

    public static function get($key, $default = null)
    {
        $config = new Configuration;

        if (is_null($config->configurations->where('key', $key)->first()->value)) {
            return $default;
        }

        return $config->configurations->where('key', $key)->first()->value;
    }
}
