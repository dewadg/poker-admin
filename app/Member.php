<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'username',
        'email',
        'full_name',
        'phone',
        'bank_id',
        'bank_account_number',
        'balance'
    ];

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
}
