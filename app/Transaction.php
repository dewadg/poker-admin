<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'type',
        'bank',
        'bank_account_number',
        'bank_account_holder',
        'note',
        'amount',
        'user_id',
        'completed_at',
        'pending_at',
        'canceled_at',
        'status',
        'member_id',
        'bank_account_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function completedBy()
    {
        return $this->belongsTo('App\User', 'completed_by');
    }

    public function setPendingBy()
    {
        return $this->belongsTo('App\User', 'set_pending_by');
    }

    public function canceledBy()
    {
        return $this->belongsTo('App\User', 'canceled_by');
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function bankAccount()
    {
        return $this->belongsTo('App\BankAccount');
    }
}
