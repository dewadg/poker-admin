<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Configuration;

class ConfigurationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Configuration::class, function () {
            return new Configuration;
        });
    }
}
