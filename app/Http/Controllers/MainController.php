<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {
        if (! Auth::check()) {
            return view('login');
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function doLogin(Request $request)
    {
        $user_data = $request->except('_token');

        if (Auth::attempt(['name' => $user_data['name'], 'password' => $user_data['password']], isset($user_data['remember']) && $user_data['remember'] ? true : false)) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->back()->withMsg('Wrong username/password');
        }
    }

    public function doLogout()
    {
        Auth::logout();

        return redirect('/');
    }

    public function dashboard()
    {
        return view('dashboard');
    }
}
