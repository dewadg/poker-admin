<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Member;

class MemberController extends Controller
{
    public function index(Request $request)
    {
        if (! empty($request->get('fields'))) {
            $fields = explode(',', $request->get('fields'));

            return Member::orderBy('username', 'ASC')->get($fields);
        }

        return Member::orderBy('username', 'ASC')->get();
    }

    public function get($id)
    {
        try {
            return Member::with('bank')->findOrFail($id);
        } catch (\Exception $e) {

        }
    }
}
