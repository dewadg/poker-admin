<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use DB;
use Validator;
use App\Traits\Transaction\DataTables;
use App\Transaction;

class WithdrawalController extends Controller
{
    use DataTables;

    protected $table = 'transactions';
    protected $fields_alias = [
        'transaction_id' => 'transactions.id',
        'member_username' => 'members.username',
        'member_full_name' => 'members.full_name',
        'member_bank_name' => 'member_banks.name',
        'member_bank_account_number' => 'members.bank_account_number',
        'user_name' => 'users.name',
        'transaction_amount' => 'transactions.amount',
        'transaction_created_at' => 'transactions.created_at',
        'transaction_status' => 'transactions.status',
        'transaction_note' => 'transactions.note'
    ];

    public function index(Request $request)
    {
        $fields = [
            'transactions.id AS transaction_id',
            'members.username AS member_username',
            'members.full_name AS member_full_name',
            'member_banks.name AS member_bank_name',
            'members.bank_account_number AS member_bank_account_number',
            'users.name AS user_name',
            'transactions.amount AS transaction_amount',
            'transactions.created_at AS transaction_created_at',
            'transactions.status AS transaction_status',
            'transactions.note AS transaction_note'
        ];
        $this->builder = Transaction::where('transactions.type', '2')
            ->select($fields)
            ->join('members', 'members.id', '=', 'transactions.member_id')
            ->join('users', 'users.id', '=', 'transactions.user_id')
            ->join('banks AS member_banks', 'member_banks.id', '=', 'members.bank_id');

        if (! empty($request->get('datatables')) && $request->get('datatables') == 1) {
            return $this->serverSideData($request);
        }

        return Transaction::where('transactions.type', '2')
            ->with(['member', 'user', 'bank'])
            ->get();
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            Transaction::where([
                'type' => '2',
                'id' => $id
            ])->delete();
            DB::commit();

            return response()->json([
                'status' => 200
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 500,
                'errors' => [
                    $e->getMessage()
                ]
            ], 500);
        }
    }

    public function bulkAction(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'action' => 'required',
            'transaction_id' => 'required|array'
        ]);

        if ($validation->passes()) {
            try {
                DB::beginTransaction();

                switch ($request->get('action')) {
                    case 1:
                        $this->massSetComplete($request->get('transaction_id'));
                        break;
                    case 2:
                        $this->massDestroy($request->get('transaction_id'));
                        break;
                }

                DB::commit();

                $response = [
                    'status' => 200,
                    'data' => [
                        'Bulk action succeed'
                    ]
                ];
            } catch (\Exception $e) {
                DB::rollback();

                $response = [
                    'status' => 500,
                    'data' => [
                        $e->getMessage()
                    ]
                ];
            }
        } else {
            $response = [
                'status' => 400,
                'errors' => $validation->errors()
            ];
        }

        return response()->json($response);
    }

    protected function massSetComplete($id)
    {
        Transaction::where('type', '2')
            ->whereIn('id', $id)
            ->update([
                'status' => 1,
                'user_id' => Auth::user()->id
            ]);
    }

    protected function massDestroy($id)
    {
        Transaction::where('type', '2')
            ->whereIn('id', $id)
            ->delete();
    }
}
