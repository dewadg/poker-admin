<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Transaction;
use App\TransactionVerification;

class ReportController extends Controller
{
    public function getByMonth(Request $request)
    {
        if (! empty($request->get('month'))) {
            $transactions = TransactionVerification::with('user')
                ->whereRaw('LEFT(date, 7) = "' . date('Y') . '-' . $request->get('month') . '"')
                ->get();

            $transactions->each(function ($item, $key) {
                $item->date = date_create($item->date)->format('j F Y');
            });

            return $transactions;
        }

        return [];
    }
}
