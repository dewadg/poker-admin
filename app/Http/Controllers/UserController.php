<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users,name',
            'type' => 'required',
            'password' => 'required',
            'password_conf' => 'required|same:password'
        ]);

        try {
            $user_data = $request->except('_token');
            $user_data['password'] = bcrypt($user_data['password']);
            unset($user_data['password_conf']);

            DB::beginTransaction();
            User::create($user_data);
            DB::commit();

            return redirect()->route('users.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
            'password' => 'sometimes:required',
            'password_conf' => 'required_with:password|same:password'
        ]);

        try {
            $user_data = $request->except('_token', '_method');
            $user_data['password'] = bcrypt($user_data['password']);
            unset($user_data['password_conf']);

            DB::beginTransaction();
            User::find($id)->update($user_data);
            DB::commit();

            return redirect()->route('users.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            User::find($id)->delete();
            DB::commit();

            return redirect()->route('users.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }
}
