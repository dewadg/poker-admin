<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\BankAccount;
use App\Bank;

class BankAccountController extends Controller
{
    public function index()
    {
        $bank_accounts = BankAccount::all();

        return view('bank-accounts.index', compact('bank_accounts'));
    }

    public function create()
    {

        $banks = Bank::orderBy('name', 'ASC')->get();

        return view('bank-accounts.create', compact('banks'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bank_id' => 'required',
            'bank_account_number' => 'required',
            'bank_account_holder' => 'required'
        ]);

        try {
            $user_data = $request->except('_token');

            DB::beginTransaction();

            BankAccount::create($user_data);

            DB::commit();

            return redirect()->route('bank-accounts.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function edit($id)
    {
        $banks = Bank::orderBy('name', 'ASC')->get();
        $bank_account = BankAccount::findOrFail($id);

        return view('bank-accounts.edit', compact('banks', 'bank_account'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bank_id' => 'required',
            'bank_account_number' => 'required',
            'bank_account_holder' => 'required'
        ]);

        try {
            $user_data = $request->except('_token', '_method');

            DB::beginTransaction();

            $bank_account = BankAccount::findOrFail($id);
            $bank_account->update($user_data);

            DB::commit();

            return redirect()->route('bank-accounts.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            BankAccount::findOrFail($id)->delete();

            DB::commit();

            return redirect()->route('bank-accounts.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }
}
