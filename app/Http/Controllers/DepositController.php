<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Excel;
use App\Transaction;
use App\Member;
use App\BankAccount;
use App\TransactionVerification;
use App\Configuration;

class DepositController extends Controller
{
    public function __construct()
    {
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $deposits = Transaction::where('type', '1')->get();

        return view('deposits.index', compact('deposits'));
    }

    public function transaction()
    {
        $members = Member::orderBy('username', 'ASC')->get();
        $bank_accounts = BankAccount::orderBy('bank_account_holder', 'ASC')->get();

        return view('deposits.transaction', compact('members', 'bank_accounts'));
    }

    public function store(Request $request)
    {
        $transactions = collect($request->get('transactions'));
        $transactions = $transactions->reject(function ($item, $key) {
            return $item['index'] == -1;
        });
        $validation = Validator::make($transactions->toArray(), [
            'transactions.*.member_id' => 'required',
            'transactions.*.bank_account_id' => 'required',
            'transactions.*.amount' => 'required',
            'transactions.*.status' => 'required'
        ]);

        if ($validation->fails()) {
            $response = [
                'status' => 500,
                'errors' => $validation->errors()
            ];

            return response()->json($response);
        } else {
            try {
                DB::beginTransaction();

                $this->checkVerifiedDate(date('Y-m-d'));

                foreach ($transactions as $transaction) {
                    $bank_account = BankAccount::with('bank')->findOrFail($transaction['bank_account_id']);
                    $transaction['type'] = '1';
                    $transaction['bank'] = $bank_account->bank->name;
                    $transaction['bank_account_number'] = $bank_account->bank_account_number;
                    $transaction['bank_account_holder'] = $bank_account->bank_account_holder;
                    $transaction['user_id'] = \Auth::user()->id;

                    if ($transaction['status'] == '1') {
                        $transaction['completed_at'] = date('Y-m-d H:i:s');

                        $member = Member::find($transaction['member_id']);
                        $member->update(['balance' => $member->balance + $transaction['amount']]);
                    } else if ($transaction['status'] == '2') {
                        $transaction['pending_at'] = date('Y-m-d H:i:s');
                    } else if ($transaction['status'] == '3') {
                        $transaction['canceled_at'] = date('Y-m-d H:i:s');
                    }

                    Transaction::create($transaction);
                }

                DB::commit();

                return response()->json([
                    'status' => 200
                ]);
            } catch (\Exception $e) {
                DB::rollback();

                if (\App::environment('local')) {
                    return response()->json($e->getMessage());
                } else {
                    $response = [
                        'status' => 500,
                        'errors' => ['Internal server error occured']
                    ];

                    return response()->json($response);
                }
            }
        }
    }

    public function view($id)
    {
        $deposit = Transaction::where(['id' => $id, 'type' => '1'])->first();

        return view('deposits.view', compact('deposit'));
    }

    public function pendingList()
    {
        $deposits = Transaction::where(['type' => '1', 'status' => '2'])->get();
        $sub_title = 'Pending';

        return view('deposits.index', compact('deposits', 'sub_title'));
    }

    public function edit($id)
    {
        $deposit = Transaction::where(['id' => $id, 'type' => '1'])->first();
        $deposit_date = date_create($deposit->created_at);
        $current_date = date_create(date('Y-m-d H:i:s'));

        if (
            \Auth::user()->type == 'Operator' &&
            $deposit->status == '1' &&
            $deposit_date->diff($current_date)->days >= Configuration::get('max_edit_days_limit')
        ) {
            abort(403);
        }

        $members = Member::orderBy('username', 'ASC')->get();
        $bank_accounts = BankAccount::orderBy('bank_account_holder', 'ASC')->get();

        return view('deposits.edit', compact('deposit', 'members', 'bank_accounts'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bank_account_id' => 'required',
            'amount' => 'required',
            'status' => 'required'
        ]);

        try {
            $user_data = $request->except('_token', '_method');
            $bank_account = BankAccount::findOrFail($user_data['bank_account_id']);
            $user_data['bank'] = $bank_account->bank->name;
            $user_data['bank_account_number'] = $bank_account->bank_account_number;
            $user_data['user_id'] = \Auth::user()->id;

            DB::beginTransaction();

            $deposit = Transaction::where(['id' => $id, 'type' => '1'])->first();
            $deposit->update($user_data);

            DB::commit();

            return redirect()->route('deposits.index')->withMsg('Transaction has been successfully edited');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Transaction::where(['type' => '1', 'id' => $id])->first()->delete();

            DB::commit();

            return redirect()->route('deposits.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function advSearch(Request $request) {
        $this->validate($request, [
            'start_date' => 'sometimes|required',
            'end_date' => 'sometimes|required'
        ]);

        $start_date = date_create($request->get('start_date') . ' 00:00:00');
        $end_date = date_create($request->get('end_date') . ' 23:59:59');
        $sub_title = 'Date Range: ' . $start_date->format('j F Y') .' to ' . $end_date->format('j F Y');
        $deposits = Transaction::whereBetween('created_at', [$start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')])
            ->where('type', '1')
            ->get();

        return view('deposits.index', compact('deposits', 'sub_title'));
    }

    private function checkVerifiedDate($date)
    {
        $checked_date = TransactionVerification::where('date', $date)->get();

        if (count($checked_date) > 0) {
            return;
        } else {
            try {
                TransactionVerification::create([
                    'date' => $date,
                    'status' => '',
                    'user_id' => \Auth::user()->id
                ]);
            } catch (\Exception $e) {
                if (\App::environment('local')) {
                    throw $e;
                } else {
                    abort(503);
                }
            }
        }
    }
}
