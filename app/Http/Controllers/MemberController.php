<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Member;
use App\Bank;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::all();

        return view('members.index', compact('members'));
    }

    public function create()
    {
        $banks = Bank::orderBy('name', 'ASC')->get();

        return view('members.create', compact('banks'));
    }

    public function isUsernameUnique(Request $request)
    {
        $foundMembers = Member::where('username', $request->get('username'))->get();

        if (count($foundMembers) == 0) {
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:members,username',
            'full_name' => 'required',
            'phone' => 'required|max:12',
            'bank_id' => 'required',
            'bank_account_number' => 'required'
        ]);

        try {
            $user_data = $request->except('_token');
            $user_data['balance'] = 0;
            unset($user_data['password_conf']);

            DB::beginTransaction();
            Member::create($user_data);
            DB::commit();

            return redirect()->route('members.index');
        } catch (\Exception $e) {
            DB::rollback();
            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function edit($id)
    {
        $member = Member::findOrFail($id);
        $banks = Bank::orderBy('name', 'ASC')->get();

        return view('members.edit', compact('member', 'banks'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'phone' => 'required|max:12',
            'bank_id' => 'required',
            'bank_account_number' => 'required'
        ]);

        try {
            $user_data = $request->except('_token', '_method');
            unset($user_data['password_conf']);

            DB::beginTransaction();

            $member = Member::findOrFail($id)->update($user_data);

            DB::commit();

            return redirect()->route('members.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Member::findOrFail($id)->delete();

            DB::commit();

            return redirect()->route('members.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
