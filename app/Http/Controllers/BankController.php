<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Bank;

class BankController extends Controller
{
    public function index()
    {
        $banks = Bank::all();

        return view('banks.index', compact('banks'));
    }

    public function create()
    {
        return view('banks.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'color' => 'required'
        ]);

        try {
            DB::beginTransaction();
            Bank::create($request->except('_token'));
            DB::commit();

            return redirect()->route('banks.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function edit($id)
    {
        $bank = Bank::findOrFail($id);

        return view('banks.edit', compact('bank'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'color' => 'required'
        ]);

        try {
            DB::beginTransaction();
            Bank::find($id)->update($request->except('_token'));
            DB::commit();

            return redirect()->route('banks.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Bank::find($id)->delete();
            DB::commit();

            return redirect()->route('banks.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }
}
