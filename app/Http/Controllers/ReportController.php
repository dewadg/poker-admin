<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Excel;
use Validator;
use App\Bank;
use App\BankAccount;
use App\Transaction;
use App\TransactionVerification;

class ReportController extends Controller
{
    public function __construct()
    {
        ini_set('memory_limit', '1024M');
    }

    public function report()
    {
        return view('reports.report');
    }

    public function generate(Request $request)
    {
        $this->validate($request, [
            'daterange' => 'required'
        ]);
        $dates = explode(' - ', $request->get('daterange'));

        Excel::create('report-' . date('Y-m-d'), function ($excel) use ($dates) {
            $output = [];
            $date_range = new \DatePeriod(
                new \DateTime($dates[0]),
                new \DateInterval('P1D'),
                new \DateTime($dates[1] . '+1 day')
            );

            foreach ($date_range as $date) {
                $excel->sheet($date->format('j F Y'), function ($sheet) use ($date) {
                    $data = [];
                    $banks = Bank::orderBy('name', 'ASC')->get();
                    $monthly_deposit = Transaction::whereRaw('LEFT(created_at, 7) = "' . $date->format('Y-m') . '"')
                        ->where('type', '1')
                        ->where('status', '!=', '3');
                    $monthly_withdrawal = Transaction::whereRaw('LEFT(created_at, 7) = "' . $date->format('Y-m') . '"')
                        ->where('type', '2')
                        ->where('status', '!=', '3');
                    $daily_deposit = $monthly_deposit
                        ->whereRaw('LEFT(created_at, 10) = "' . $date->format('Y-m-d') . '"');
                    $daily_withdrawal = $monthly_withdrawal
                        ->whereRaw('LEFT(created_at, 10) = "' . $date->format('Y-m-d') . '"');

                    $monthly_deposit_amount = $monthly_deposit->sum('amount');
                    $monthly_withdrawal_amount = $monthly_withdrawal->sum('amount');
                    $daily_deposit_amount = $daily_deposit->sum('amount');
                    $daily_withdrawal_amount = $daily_withdrawal->sum('amount');

                    $daily_deposit = $daily_deposit->get();
                    $daily_withdrawal = $daily_withdrawal->get();

                    $monthly_hit = ($monthly_deposit_amount - $monthly_withdrawal_amount) / 1000;
                    $daily_hit = ($daily_deposit_amount - $daily_withdrawal_amount) / 1000;
                    $row_count = count($daily_deposit) >= count($daily_withdrawal) ? count($daily_deposit) : count($daily_withdrawal);
                    $d_bank_pos = [];
                    $w_bank_pos = [];

                    $sheet->cell('A1', function ($cell) use ($date) {
                        $cell->setValue('Transactions Report | ' . $date->format('j F Y'));
                        $cell->setFontSize(16);
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');

                        $verification = TransactionVerification::where('date', $date->format('Y-m-d'))->first();

                        if (! empty($verification) && $verification->status == 'green') {
                            $cell->setBackground('#00a65a');
                        } else if (! empty($verification) && $verification->status == 'red') {
                            $cell->setBackground('#e74c3c');
                        }
                    });
                    $sheet->mergeCells('A1:D1');
                    $sheet->setHeight(1, 50);
                    $sheet->setHeight(2, 40);

                    $sheet->cell('E1', function ($cell) use ($date, $daily_hit) {
                        $cell->setValue('Daily Hit: ' . $daily_hit);
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                    });

                    $sheet->cell('F1', function ($cell) use ($date, $monthly_hit) {
                        $cell->setValue('Monthly Hit: ' . $monthly_hit);
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->mergeCells('F1:G1');

                    $sheet->cell('J1', function ($cell) use ($daily_deposit_amount) {
                        $cell->setValue('Daily Deposits: ' . ($daily_deposit_amount / 1000));
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                    });

                    $sheet->cell('K1', function ($cell) use ($daily_withdrawal_amount) {
                        $cell->setValue('Daily Withdrawals: ' . ($daily_withdrawal_amount / 1000));
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                    });

                    /* Table headers START */
                    $d_start_from = 'E';
                    $d_bank_totals = [];
                    $w_bank_totals = [];

                    foreach ($banks as $bank) {
                        $d_start_from++;

                        $d_bank_pos[] = [
                            $bank->name,
                            $bank->color,
                            $d_start_from
                        ];

                        $sheet->cell($d_start_from . '3', function ($cell) use ($bank) {
                            $cell->setValue($bank->name);
                            $cell->setAlignment('center');
                            $cell->setValignment('center');
                            $cell->setBackground('#9900ff');
                            $cell->setFontColor('#ffffff');
                        });

                        $total_amount = $daily_deposit
                            ->where('bank', $bank->name)
                            ->sum('amount');

                        array_push($d_bank_totals, [$total_amount, $bank->name]);

                        $total_amount = $daily_withdrawal
                            ->where('bank', $bank->name)
                            ->sum('amount');

                        array_push($w_bank_totals, [$total_amount, $bank->name]);
                    }

                    $sheet->cell('A2', function ($cell) {
                        $cell->setValue('NO');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('A2:A3');
                    $sheet->setSize('A2', 5);

                    $sheet->cell('B2', function ($cell) {
                        $cell->setValue('MEMBER');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('B2:B3');
                    $sheet->setSize('B2', 20);

                    $sheet->cell('C2', function ($cell) {
                        $cell->setValue('BANK ACCOUNT');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('C2:C3');
                    $sheet->setSize('C2', 25);

                    $sheet->cell('D2', function ($cell) {
                        $cell->setValue('NOTE');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('D2:D3');
                    $sheet->setSize('D2', 30);

                    $sheet->cell('E2', function ($cell) {
                        $cell->setValue('PROCESSED BY');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('E2:E3');
                    $sheet->setSize('E2', 20);

                    $sheet->cell('F2', function ($cell) {
                        $cell->setValue('DEPOSITS');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells('F2:' . $d_start_from . '2');
                    $d_stop_at = $d_start_from;

                    // Withdrawals ==>
                    $d_start_from++;
                    $d_start_from++;
                    $w_start_from = $d_start_from;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('NO');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells($d_start_from . '2:' . $d_start_from . '3');
                    $sheet->setSize($d_start_from . '2', 5);

                    $d_start_from++;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('MEMBER');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells($d_start_from . '2:' . $d_start_from . '3');
                    $sheet->setSize($d_start_from . '2', 20);

                    $d_start_from++;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('BANK ACCOUNT');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells($d_start_from . '2:' . $d_start_from . '3');
                    $sheet->setSize($d_start_from . '2', 25);

                    $d_start_from++;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('NOTE');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells($d_start_from . '2:' . $d_start_from . '3');
                    $sheet->setSize($d_start_from . '2', 30);

                    $d_start_from++;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('PROCESSED BY');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->mergeCells($d_start_from . '2:' . $d_start_from . '3');
                    $sheet->setSize($d_start_from . '2', 20);

                    $d_start_from++;
                    $sheet->cell($d_start_from . '2', function ($cell) {
                        $cell->setValue('WITHDRAWALS');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setBackground('#9900ff');
                        $cell->setFontColor('#ffffff');
                    });
                    $w_b_start_from = $d_start_from;

                    $banks_count = count($banks);
                    $counter = 0;

                    foreach ($banks as $bank) {
                        $w_bank_pos[] = [
                            $bank->name,
                            $bank->color,
                            $d_start_from
                        ];

                        $sheet->cell($d_start_from . '3', function ($cell) use ($bank) {
                            $cell->setValue($bank->name);
                            $cell->setAlignment('center');
                            $cell->setValignment('center');
                            $cell->setBackground('#9900ff');
                            $cell->setFontColor('#ffffff');
                        });

                        $counter++;

                        if ($counter == $banks_count) {
                            break;
                        }

                        $d_start_from++;
                    }

                    $sheet->mergeCells($w_b_start_from . '2:' . $d_start_from . '2');
                    /* Table headers END */

                    for ($row = 4, $d_row = 4, $w_row = 4, $i = 0; $i < $row_count; $row++, $i++) {
                        /* Generate data row START */
                        if (isset($daily_deposit[$i])) {
                            $d_row++;
                        }

                        $data = [
                            isset($daily_deposit[$i]) ? $i + 1 : null,
                            isset($daily_deposit[$i]) ? $daily_deposit[$i]->member->username : null,
                            isset($daily_deposit[$i]) ? $daily_deposit[$i]->member->full_name : null,
                            isset($daily_deposit[$i]) ? $daily_deposit[$i]->note : null,
                            isset($daily_deposit[$i]) ? $daily_deposit[$i]->user->name : null
                        ];

                        foreach ($banks as $bank) {
                            $data[] = isset($daily_deposit[$i]) ? ($daily_deposit[$i]->bank == $bank->name ? $daily_deposit[$i]->amount / 1000 : null) : null;
                        }

                        if (isset($daily_withdrawal[$i])) {
                            $w_row++;
                        }

                        $data[] = null;
                        $data[] = isset($daily_withdrawal[$i]) ? $i + 1 : null;
                        $data[] = isset($daily_withdrawal[$i]) ? $daily_withdrawal[$i]->member->username : null;
                        $data[] = isset($daily_withdrawal[$i]) ? $daily_withdrawal[$i]->member->full_name : null;
                        $data[] = isset($daily_withdrawal[$i]) ? $daily_withdrawal[$i]->note : null;
                        $data[] = isset($daily_withdrawal[$i]) ? $daily_withdrawal[$i]->user->name : null;

                        foreach ($banks as $bank) {
                            $data[] = isset($daily_withdrawal[$i]) ? ($daily_withdrawal[$i]->bank == $bank->name ? $daily_withdrawal[$i]->amount / 1000 : null) : null;
                        }
                        /* Generate data row END */

                        $sheet->row($row, $data);

                        if (isset($daily_deposit[$i]) && $daily_deposit[$i]->status == '2') {
                            $sheet->cells('A' . $row . ':' . $d_stop_at . $row, function ($cells) {
                                $cells->setBackground('#F9BF3B');
                            });
                        }

                        if (isset($daily_withdrawal[$i]) && $daily_withdrawal[$i]->status == '2') {
                            $sheet->cells($w_start_from . $row . ':' . $d_start_from . $row, function ($cells) {
                                $cells->setBackground('#F9BF3B');
                            });
                        }
                    }

                    //

                    $i = 0;

                    foreach ($d_bank_pos as $pos) {
                        $sheet->cells($pos[2] . '4' . ':' . $pos[2] . $row, function ($cells) use ($pos) {
                            $cells->setFontColor($pos[1]);
                        });

                        $sheet->cell($pos[2] . ($d_row + 1), function ($cell) use ($d_bank_totals, $i, $pos) {
                            $cell->setValue(isset($d_bank_totals[$i]) ? ($d_bank_totals[$i][0] / 1000) : 0);
                            $cell->setFontColor($pos[1]);
                        });

                        $i++;
                    }

                    $i = 0;

                    foreach ($w_bank_pos as $pos) {
                        $sheet->cells($pos[2] . '4' . ':' . $pos[2] . $row, function ($cells) use ($pos) {
                            $cells->setFontColor($pos[1]);
                        });

                        $sheet->cell($pos[2] . ($w_row + 1), function ($cell) use ($w_bank_totals, $i, $pos) {
                            $cell->setValue(isset($w_bank_totals[$i]) ? ($w_bank_totals[$i][0] / 1000) : 0);
                            $cell->setFontColor($pos[1]);
                        });

                        $i++;
                    }

                    $sheet->setBorder('A2:' . $d_start_from . $row, 'thin', 'thin', 'thin', 'thin');
                    $sheet->getStyle('A1:' . $d_start_from . '2')
                        ->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                            ]
                        ]);
                });
            }
        })->download('xls');
    }

    public function verification()
    {
        $months = [
            ['01', 'January'],
            ['02', 'February'],
            ['03', 'March'],
            ['04', 'April'],
            ['05', 'May'],
            ['06', 'June'],
            ['07', 'July'],
            ['08', 'August'],
            ['09', 'September'],
            ['10', 'October'],
            ['11', 'November'],
            ['12', 'December'],
        ];

        return view('reports.verification', compact('months'));
    }

    public function verifyTransactions(Request $request)
    {
        $this->validate($request, [
            'transactions' => 'required|array'
        ]);

        $transactions = $request->get('transactions');

        DB::beginTransaction();

        try {
            foreach ($transactions as $transaction) {
                TransactionVerification::find($transaction['id'])
                    ->update([
                        'status' => isset($transaction['status']) ? $transaction['status'] : null,
                        'user_id' => \Auth::user()->id
                    ]);
            }

            DB::commit();

            return redirect()->route('reports.verification');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }
}
