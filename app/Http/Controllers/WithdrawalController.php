<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use App\Transaction;
use App\Member;
use App\BankAccount;
use App\TransactionVerification;
use App\Configuration;

class WithdrawalController extends Controller
{
    public function __construct()
    {
        ini_set('memory_limit', '1024M');
    }

    public function index()
    {
        $withdrawals = Transaction::where('type', '2')->get();

        return view('withdrawals.index', compact('withdrawals'));
    }

    public function transaction()
    {
        $members = Member::orderBy('username', 'ASC')->get();
        $bank_accounts = BankAccount::orderBy('bank_account_holder', 'ASC')->get();

        return view('withdrawals.transaction', compact('members', 'bank_accounts'));
    }

    public function store(Request $request)
    {
        $transactions = collect($request->get('transactions'));
        $transactions = $transactions->reject(function ($item, $key) {
            return $item['index'] == -1;
        });
        $validation = Validator::make($transactions->toArray(), [
            'transactions.*.member_id' => 'required',
            'transactions.*.bank_account_id' => 'required',
            'transactions.*.amount' => 'required',
            'transactions.*.status' => 'required'
        ]);

        if ($validation->fails()) {
            $response = [
                'status' => 500,
                'errors' => $validation->errors()
            ];

            return response()->json($response);
        } else {
            try {
                DB::beginTransaction();

                $this->checkVerifiedDate(date('Y-m-d'));

                foreach ($transactions as $transaction) {
                    $bank_account = BankAccount::with('bank')->findOrFail($transaction['bank_account_id']);
                    $transaction['type'] = '2';
                    $transaction['bank'] = $bank_account->bank->name;
                    $transaction['bank_account_number'] = $bank_account->bank_account_number;
                    $transaction['bank_account_holder'] = $bank_account->bank_account_holder;
                    $transaction['user_id'] = \Auth::user()->id;

                    if ($transaction['status'] == '1') {
                        $transaction['completed_at'] = date('Y-m-d H:i:s');
                    } else if ($transaction['status'] == '2') {
                        $transaction['pending_at'] = date('Y-m-d H:i:s');
                    } else if ($transaction['status'] == '3') {
                        $transaction['canceled_at'] = date('Y-m-d H:i:s');
                    }

                    Transaction::create($transaction);

                    if ($transaction['status'] == '1') {
                        $member = Member::find($transaction['member_id']);
                        $member->update(['balance' => $member->balance - $transaction['amount']]);
                    }
                }

                DB::commit();

                return response()->json([
                    'status' => 200
                ]);
            } catch (\Exception $e) {
                DB::rollback();

                if (\App::environment('local')) {
                    return response()->json($e->getMessage());
                } else {
                    $response = [
                        'status' => 500,
                        'errors' => ['Internal server error occured']
                    ];

                    return response()->json($response);
                }
            }
        }
    }

    public function view($id)
    {
        $withdrawal = Transaction::where(['id' => $id, 'type' => '2'])->first();

        return view('withdrawals.view', compact('withdrawal'));
    }

    public function pendingList()
    {
        $withdrawals = Transaction::where(['type' => '2', 'status' => '2'])->get();
        $sub_title = 'Pending';

        return view('withdrawals.index', compact('withdrawals', 'sub_title'));
    }

    public function edit($id)
    {
        $withdrawal = Transaction::where(['id' => $id, 'type' => '2'])->first();
        $withdrawal_date = date_create($withdrawal->created_at);
        $current_date = date_create(date('Y-m-d H:i:s'));

        if (
            \Auth::user()->type == 'Operator' &&
            $withdrawal->status == '1' &&
            $withdrawal_date->diff($current_date)->days >= Configuration::get('max_edit_days_limit')
        ) {
            abort(403);
        }

        $members = Member::orderBy('username', 'ASC')->get();
        $bank_accounts = BankAccount::orderBy('bank_account_holder', 'ASC')->get();

        return view('withdrawals.edit', compact('withdrawal', 'members', 'bank_accounts'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'member_id' => 'required',
            'bank_account_id' => 'required',
            'amount' => 'required'
        ]);

        try {
            $user_data = $request->except('_token', '_method');
            $bank_account = BankAccount::findOrFail($user_data['bank_account_id']);
            $user_data['bank'] = $bank_account->bank->name;
            $user_data['bank_account_number'] = $bank_account->bank_account_number;
            $user_data['user_id'] = \Auth::user()->id;

            DB::beginTransaction();

            $withdrawal = Transaction::where(['id' => $id, 'type' => '2'])->first();
            $withdrawal->update($user_data);

            DB::commit();

            return redirect()->route('withdrawals.index')->withMsg('Transaction has been successfully edited');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Transaction::where(['type' => '2', 'id' => $id])->first()->delete();

            DB::commit();

            return redirect()->route('withdrawals.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function advSearch(Request $request) {
        $this->validate($request, [
            'start_date' => 'sometimes|required',
            'end_date' => 'sometimes|required'
        ]);

        $start_date = date_create($request->get('start_date') . ' 00:00:00');
        $end_date = date_create($request->get('end_date') . ' 23:59:59');
        $sub_title = 'Date Range: ' . $start_date->format('j F Y') .' to ' . $end_date->format('j F Y');
        $withdrawals = Transaction::whereBetween('created_at', [$start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')])
            ->where('type', '2')
            ->get();

        return view('withdrawals.index', compact('withdrawals', 'sub_title'));
    }

    public function bulkAction(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'action' => 'required',
            'withdrawal_id' => 'required|array'
        ]);

        if ($validation->passes()) {
            try {
                DB::beginTransaction();

                switch ($request->get('action')) {
                    case 1:
                        $this->massSetComplete($request->get('withdrawal_id'));
                        break;
                    case 2:
                        $this->massDestroy($request->get('withdrawal_id'));
                        break;
                }

                DB::commit();

                $response = [
                    'status' => 200,
                    'data' => [
                        'Delete bulk action succeed'
                    ]
                ];
            } catch (\Exception $e) {
                DB::rollback();

                $response = [
                    'status' => 500,
                    'data' => [
                        'Bulk action failed'
                    ]
                ];
            }
        } else {
            $response = [
                'status' => 400,
                'errors' => $validation->errors()
            ];
        }

        return response()->json($response);
    }

    public function massSetComplete($id)
    {
        Transaction::where('type', '2')
            ->whereIn('id', $id)
            ->update([
                'status' => 1,
                'user_id' => \Auth::user()->id
            ]);
    }

    public function massDestroy($id)
    {
        Transaction::where('type', '2')
            ->whereIn('id', $id)
            ->delete();
    }

    private function checkVerifiedDate($date)
    {
        $checked_date = TransactionVerification::where('date', $date)->get();

        if (count($checked_date) > 0) {
            return;
        } else {
            try {
                TransactionVerification::create([
                    'date' => $date,
                    'status' => '',
                    'user_id' => \Auth::user()->id
                ]);
            } catch (\Exception $e) {
                if (\App::environment('local')) {
                    throw $e;
                } else {
                    abort(503);
                }
            }
        }
    }
}
