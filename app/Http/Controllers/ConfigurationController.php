<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Configuration;

class ConfigurationController extends Controller
{
    public function index()
    {
        $configurations = Configuration::all();

        return view('configurations.index', compact('configurations'));
    }

    public function create()
    {
        return view('configurations.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|unique:configurations,key',
            'value' => 'required'
        ]);

        DB::beginTransaction();

        try {
            Configuration::create($request->all());

            DB::commit();

            return redirect()->route('configurations.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function edit($id)
    {
        $configuration = Configuration::findOrFail($id);

        return view('configurations.edit', compact('configuration'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value' => 'required'
        ]);

        DB::beginTransaction();

        try {
            Configuration::findOrFail($id)
                ->update([
                    'value' => $request->get('value')
                ]);

            DB::commit();

            return redirect()->route('configurations.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            Configuration::findOrFail($id)
                ->delete();

            DB::commit();

            return redirect()->route('configurations.index');
        } catch (\Exception $e) {
            DB::rollback();

            if (\App::environment('local')) {
                throw $e;
            } else {
                abort(500);
            }
        }
    }
}
