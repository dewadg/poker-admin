<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->type == 'Administrator') {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
