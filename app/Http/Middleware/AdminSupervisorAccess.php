<?php

namespace App\Http\Middleware;

use Closure;

class AdminSupervisorAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->type == 'Administrator' || \Auth::user()->type == 'Supervisor') {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
