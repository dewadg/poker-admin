<?php

namespace App\Traits\Transaction;

use Illuminate\Http\Request;
use DB;

trait DataTables
{
    protected $records_total;
    protected $records_filtered;
    protected $records;
    protected $request;

    protected function serverSideData(Request $request)
    {
        $this->request = $request;
        $this->records = $this->builder;
        $this->records_total = $this->builder->get()->count();
        $this->records_filtered = $this->records_total;

        if (! empty($this->request->get('status'))) {
            $this->getByStatus();
        }

        if (! empty($request->get('daterange'))) {
            $this->getByDateRange();
        }

        if (! empty($this->request->get('search')['value'])) {
            $this->search();
        }

        $this->records_total = $this->records->count();
        $this->records_filtered = $this->records_total;

        $this->records = $this->records
            ->skip($this->request->get('start'))
            ->take($this->request->get('length'))
            ->orderBy('transactions.created_at', 'DESC')
            ->get();

        return response()->json([
            'draw' => (! empty($this->request->get('draw')) ? $this->request->get('draw') : 1),
            'recordsTotal' => $this->records_total,
            'recordsFiltered' => $this->records_filtered,
            'data' => $this->records
        ]);
    }

    protected function search()
    {
        $i = 0;

        $this->records->where(function ($query) use ($i) {
            foreach ($this->request->get('columns') as $column) {
                $column_name = isset($this->fields_alias[$column['data']]) ? $this->fields_alias[$column['data']] : $column['data'];

                if ($i == 0) {
                    $query->where($column_name, 'LIKE', '%' . $this->request->get('search')['value'] . '%');
                } else {
                    $query->orWhere($column_name, 'LIKE', '%' . $this->request->get('search')['value'] . '%');
                }

                $i++;
            }
        });
    }

    protected function getByStatus()
    {
        if ($this->request->get('status') != '*') {
            $this->records->where('status', $this->request->get('status'));
        }
    }

    protected function getByDateRange()
    {
        $daterange = explode(' - ', $this->request->get('daterange'));
        $daterange[0] = date_create($daterange[0])->format('Y-m-d');
        $daterange[1] = date_create($daterange[1])->format('Y-m-d');

        $this->records->whereRaw('LEFT(transactions.created_at, 10) BETWEEN "' . $daterange[0] . '" AND "' . $daterange[1] . '"');
    }
}
