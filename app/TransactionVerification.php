<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionVerification extends Model
{
    protected $fillable = [
        'date',
        'status',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
