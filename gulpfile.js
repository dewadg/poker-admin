const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.combine([
        './public/bower_components/sweetalert/dist/sweetalert.css',
        './public/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css',
        './node_modules/ui-select/dist/select.min.css',
        './public/bower_components/AdminLTE/plugins/select2/select2.min.css',
        './public/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.css',
        './resources/assets/css/style.css',
    ], './public/css/plugins.css')
        .scripts([
            './node_modules/jquery/dist/jquery.min.js',
            './public/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js',
            './public/bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js',
            './public/bower_components/AdminLTE/plugins/fastclick/fastclick.js',
            './public/bower_components/sweetalert/dist/sweetalert.min.js',
            './public/bower_components/AdminLTE/dist/js/app.min.js',
            './node_modules/datatables/media/js/jquery.dataTables.min.js',
            './public/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js',
            './public/bower_components/AdminLTE/plugins/select2/select2.full.min.js',
            './node_modules/moment/moment.js',
            './public/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js',
            './node_modules/numeral/numeral.js'
        ], './public/js/plugins.js')
        .webpack('withdrawal.js')
        .webpack('deposit.js')
});
